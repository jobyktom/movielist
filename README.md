## How to run

run 'npm Install' to install the node modules
Run `npm start` or `yarn start` to run the application in development mode.
The app will start on the port 3000. Open [http://localhost:3000](http://localhost:3000) from your browser to view it.
