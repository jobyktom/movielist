// These constants are used for all async actions
export const STATUS_START = 'STATUS_START';
export const STATUS_ERROR = 'STATUS_ERROR';
export const STATUS_SUCCESS = 'STATUS_SUCCESS';

export const FETCH_MOVIES = 'FETCH_MOVIES';
export const SET_RATING_FILTER = 'SET_RATING_FILTER';
