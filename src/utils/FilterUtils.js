
export const satisfiesRatingFilter = (rating = 3, movie) => {
  return movie.voteAverage >= rating;
}

/**
 * Filters the movies based on the given filter.
 * @param movies to filter.
 * @param filter to filter the movies against.
 * @returns {*}
 */
export const filterMovies = (movies, filter) => {
  if (!movies) {
    return movies;
  }
  return movies.filter(movie => (
    satisfiesRatingFilter(filter.rating, movie))
  );
}


