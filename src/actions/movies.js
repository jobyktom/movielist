import {
  FETCH_MOVIES,
  STATUS_START,
  STATUS_SUCCESS,
  STATUS_ERROR
} from "../constants/actions";
import api from "../api";

export function fetchMovies() {
  return dispatch => {
    dispatch({
      type: FETCH_MOVIES,
      status: STATUS_START,
      payload: {}
    });

    api.getNowPlayingMovies().then(
      response => {
        dispatch({
          type: FETCH_MOVIES,
          status: STATUS_SUCCESS,
          payload: { results: response.data.results }
        });
      },
      error =>
        dispatch({
          type: FETCH_MOVIES,
          status: STATUS_ERROR,
          payload: { error }
        })
    );
  };
}
