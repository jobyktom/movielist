import {
  SET_RATING_FILTER,
} from '../constants/actions'

export function setRatingFilter(rating) {
  return (dispatch) => {
    dispatch({
      type: SET_RATING_FILTER,
      payload: {
        rating,
      },
    });
  };
}
