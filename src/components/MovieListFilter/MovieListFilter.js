import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { setRatingFilter } from "../../actions/ui";
import "./MovieListFilter.css";

/**
 * Displays and updates the movie genre and rating filters.
 */
export class MovieListFilter extends Component {
  static propTypes = {
    availableGenreIds: PropTypes.arrayOf(PropTypes.number),
    movieFilter: PropTypes.object.isRequired,
    setRatingFilter: PropTypes.func.isRequired
  };

  onRatingChange(rating) {
    if (rating < 0 || rating > 10) {
      return;
    }
    this.props.setRatingFilter(rating);
  }

  render() {
    const { movieFilter } = this.props;

    return (
      <div className="movie-list-filter">
        <div className="filter-container">
          <h3>Filter by rating</h3>
          <input
            value={movieFilter.rating}
            onChange={e => this.onRatingChange(e.target.value)}
            type="number"
            name="height"
            placeholder="Select rating"
            step="0.5"
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    movies: state.movies.moviesNowPlaying.list,
    movieFilter: state.ui.movieFilter
  };
};

const mapDispatchToProps = {
  setRatingFilter
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MovieListFilter);
